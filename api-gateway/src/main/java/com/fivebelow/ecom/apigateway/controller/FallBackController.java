package com.fivebelow.ecom.apigateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallBackController {

    @GetMapping("/userServiceFallBack")
    public String userService(){
        return "User Service Is taking longer than expected. Please try again later";
    }

    @GetMapping("/cartServiceFallBack")
    public String cartService(){
        return "Cart Service Is taking longer than expected. Please try again later";
    }
}
