# README #

### What is this repository for? ###

* This is a reference project for new microservers that any team would like to build with in FiveBelow.

### How do I get set up? ###

This is a collection of 5 small microservices 
* user-service
    - Simple service to add a user
* cart-service
    - Simple Service to add an item to cart 
* cloud-config-server
    - Config server to host the config files in a central location
* api-gateway
    - Abstract the service inforamtion define the routing based on url 
* servcice-registry
    - To enable client-side load-balancing and decouple service providers from consumers without the need for DNS.
* hystrix-dashboard
    - Used to monitor the different metrics of all the micro services on a dashboard

### Local Setup
Clone the repo and each service can be started from intelliJ
or use ```gradle bootRun``` command from command prompt 

### Additional lib or servcies used ###

Have used [Zipkin](https://zipkin.io/pages/quickstart.html) for distributed tracing. Download the [jar](https://search.maven.org/remote_content?g=io.zipkin&a=zipkin-server&v=LATEST&c=exec) and use ```java -jar zipkin.jar``` to run in your local.


### Who do I talk to? ###

* Reach out to palani.kumar@fivebelow.com for any questions