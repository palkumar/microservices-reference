package com.fivebelow.ecom.cart.service;

import com.fivebelow.ecom.cart.entity.Cart;
import com.fivebelow.ecom.cart.repository.CartRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CartService {

    @Autowired
    private CartRepository cartRepository;

    public Cart saveCart(Cart cart) {
        return cartRepository.save(cart);
    }

    public Cart findCartById(Long cartId) {
        return cartRepository.findByCartId(cartId);
    }
}
