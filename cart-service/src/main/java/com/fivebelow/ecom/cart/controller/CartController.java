package com.fivebelow.ecom.cart.controller;

import com.fivebelow.ecom.cart.entity.Cart;
import com.fivebelow.ecom.cart.service.CartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cart")
@Slf4j
public class CartController {

    @Autowired
    private CartService cartService;

    @PostMapping("/")
    public Cart saveCart(@RequestBody Cart cart){
        log.info("Saving Cart Details");
        return cartService.saveCart(cart);
    }

    @GetMapping("/{id}")
    public Cart findDepartmentById(@PathVariable("id") Long cartId) {
        log.info("Fetching Cart Details");
        return cartService.findCartById(cartId);
    }


}
