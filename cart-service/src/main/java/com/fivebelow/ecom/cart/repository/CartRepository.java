package com.fivebelow.ecom.cart.repository;
import com.fivebelow.ecom.cart.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart,Long> {

    Cart findByCartId(Long cartId);
}
