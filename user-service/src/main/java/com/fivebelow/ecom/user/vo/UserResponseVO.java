package com.fivebelow.ecom.user.vo;

import com.fivebelow.ecom.user.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseVO {
    private User user;
    private Cart cart;
}
