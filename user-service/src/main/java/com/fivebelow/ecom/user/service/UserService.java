package com.fivebelow.ecom.user.service;

import com.fivebelow.ecom.user.entity.User;
import com.fivebelow.ecom.user.repository.UserRepository;
import com.fivebelow.ecom.user.vo.Cart;
import com.fivebelow.ecom.user.vo.UserResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RestTemplate restTemplate;

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public UserResponseVO getUserCartDetails(Long userId) {
        log.info("Inside getUserCartDetails of UserService");
        UserResponseVO vo = new UserResponseVO();
        User user = userRepository.findByUserId(userId);

        Cart cart = restTemplate
                .getForObject("http://CART-SERVICE/cart/"+user.getCartId(),Cart.class);

        vo.setUser(user);
        vo.setCart(cart);
        return  vo;
    }

}
