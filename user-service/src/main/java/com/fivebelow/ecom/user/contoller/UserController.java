package com.fivebelow.ecom.user.contoller;

import com.fivebelow.ecom.user.entity.User;
import com.fivebelow.ecom.user.service.UserService;
import com.fivebelow.ecom.user.vo.UserResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/")
    public User saveUser(@RequestBody User user) {
        log.info("Saving User Details");
        return userService.saveUser(user);
    }

    @GetMapping("/{id}")
    public UserResponseVO getUserCartDetails(@PathVariable("id") Long userId) {
        log.info("Get user by id {}",userId);
        return userService.getUserCartDetails(userId);
    }
}
